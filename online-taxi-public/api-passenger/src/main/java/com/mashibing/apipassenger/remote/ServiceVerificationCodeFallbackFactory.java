package com.mashibing.apipassenger.remote;

import com.mashibing.internalcommon.dto.ResponseResult;
import com.mashibing.internalcommon.responese.NumberCodeResponse;
import org.springframework.cloud.openfeign.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class ServiceVerificationCodeFallbackFactory implements FallbackFactory<ServiceVefificationcodeClient> {

    @Override
    public ServiceVefificationcodeClient create(Throwable cause) {

        cause.printStackTrace();
        return new ServiceVefificationcodeClient() {
            @Override
            public ResponseResult<NumberCodeResponse> getNumberCode(int size , String tag) {
                NumberCodeResponse numberCodeResponse = new NumberCodeResponse();
                numberCodeResponse.setNumberCode("111111");
                return ResponseResult.success(numberCodeResponse);
            }
        };
    }
}
